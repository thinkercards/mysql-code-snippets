/* SQL Practice 1:- Select all columns and all rows of the table */

/*Consider products table used for this Practice*/

SELECT `supplier_ids`, `id`, `product_code`, `product_name`, `description`, `standard_cost`, `list_price`, `reorder_level`, `target_level`, `quantity_per_unit`, `discontinued`, `minimum_reorder_quantity`, `category`, `attachments` FROM `products`;